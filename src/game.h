#include <stdio.h>
#include <random.h>

int generatePopulation();

int generateAcres();

int generateHarvest(int situation);

int calculateSeeds(int population, int acres, int harvest);

