#include <stdio.h>
#include <random.h>
#include <stdlib.h>

int randomInteger(int min, int max)
{
  static int Init = 0;
  int rc;
  
  if (Init == 0)
    {
      /*
       *  As Init is static, it will remember it's value between
       *  function calls.  We only want srand() run once, so this
       *  is a simple way to ensure that happens.
       */
      srand(time(NULL));
      Init = 1;
    }

  /*
   * Formula:  
   *    rand() % N   <- To get a number between 0 - N-1
   *    Then add the result to min, giving you 
   *    a random number between min - max.
   */  
  rc = (rand() % (max - min + 1) + min);
  
  return (rc);
}


