#include <stdio.h>
#include <gameinfo.h>

void showHeader(){

    printf("=============================The Hammurabi Code=================================\n");
    printf("||Test your luck trying to rule an ancient civilization for a 10 years term.  ||\n");
    printf("||The Hammurabi Code game lasts 10 years and every year the player will deter-||\n");
    printf("||mine how to allocate resources for next the following year.                 ||\n");
    printf("================================================================================\n");
    printf("\n\n");
    printf("===Rules========================================================================\n");
    printf("||- The game lasts 10 years.                                                  ||\n");
    printf("||- Every year you are allowed to allocate your resorces anyway you like.     ||\n");
    printf("||- Each person needs at least 12 bags of wheat to survive and may plow 20    ||\n");
    printf("||acres a year.                                                               ||\n");
    printf("||- Every acre needs 22 bags of seeds to be plowed.                           ||\n");
    printf("||- The prices of the acre may be between 17 and 26 bags of wheat depending of||\n");
    printf("||the situation of your civilization.                                         ||\n");
    printf("||- If the people don't approve your government you might rule less than 10   ||\n");
    printf("||years.                                                                      ||\n");
    printf("||- If you get to year eleven you will be classified.                         ||\n");
    printf("================================================================================\n");
    printf("\n\n");
}


void showEnd(int end){
    switch (end) {
        case 0:
          printf("Because of your mismanagement the people prevented you from rule and thrown you from your palace.\n");
            break;
        case 1:
            printf("Your performance was reasonable. It was not bad in general. Many citizens of the population would like to see you murdered, but who do not have problems, right?\n");
            break;
        case 2:
            printf("You ruled with steel hands. Some of the remaining people see you as bad manager, but others actualy like you.\n");
            break;
        case 3:
            printf("Your performance was fantastic, others would not have done better.\n");
            break;
        default:
            printf("Sorry, but something might have gone wrong during your government.\n");
            break;
    }

}
