#include <stdio.h>
#include <game.h>
#include <random.h>

int generatePopulation(){
  return randomInteger(100, 200);
}

int generateAcres(){
  return randomInteger(1500,2000);
}

int generateHarvest(int situation){
  if (situation == 1)
    return randomInteger(22,26);
  else if (situation == 0)
    return randomInteger(17,22);
  else
    return randomInteger(17,26);
}

int calculateSeeds(int population, int acres, int harvest){
  if (acres/population<20){
    return acres * harvest;
  }
  
  return (population * 20) * harvest;
}
