#include <stdio.h>
#include <random.h>
#include <game.h>
#include <gameinfo.h>

int main(){

  int score = 0;
  int year = 1;
  int population = generatePopulation();
  int tempacres = 0;
  int acres = generateAcres();
  int harvest = generateHarvest(2);
  int bags = calculateSeeds(population, acres, harvest);
  int feed = 0;

  showHeader();
  while (year <= 10){
    printf("================Situation=================\n");
    printf("|| Year: %2d                             ||\n", year);
    printf("|| Population: %d people               ||\n", population);
    printf("|| Acres: %d                          ||\n", acres);
    printf("|| Harvest: You have %d bags of wheat||\n", bags);
    printf("|| Price of the acre: %d bags of wheat  ||\n", harvest);
    printf("==========================================\n\n");

    printf("Would you like to buy or sell acres? (Just type the ammount of acres you wish to buy, negative numbers are used to sell your acres and zero is to continue)");
    scanf("%d", &tempacres);
    
    printf("How many bags of wheat are you going to give your population this year? (Each person needs at least 12 bags to survive)\n");
    scanf("%d", &feed);

    //Analysis of the turn
    //Analyse of the acres
    int temp = tempacres * harvest;
    acres += tempacres;
    bags += temp;

    //Analyse of the bags
    if (feed/population < 10){
      showEnd(0);
      break;
    } else if (feed/population < 12 ){
      score--;
    } else {
      score++;
    }

    year++;
  }

  return 0;
}
